package tspnow;

import java.util.Observable;

/**
 * Common methods all solvers share,
 *
 * @author Paul Moggridge
 * @version 4.0.0.1 se
 */
public class Solver extends Observable {

  private int id;
  private int progress = 100;  //percent through the task
  private int runs;      // the number for times to excute
  private Best runBest;

  //problem map to process
  private CityMap map;

  public Solver(int id, CityMap map, int runs, Best runBest) {
    this.id = id;
    this.map = map;
    this.runs = runs;
    this.runBest = runBest;
  }

  /**
   * Returns the id of this solver
   *
   * @return int id
   */
  public int getID() {
    return id;
  }

  /**
   * Returns map
   *
   * @return CityMap to solve
   */
  public CityMap getMap() {
    return map;
  }

  public void setProgress(int p) {
    if ((p - progress) >= 1) {
      setChanged();
      notifyObservers("status");
    }
    progress = p;
  }

  public int getProgress() {
    return progress;
  }

  public void setRuns(int r) {
    runs = r;
    setChanged();
    notifyObservers("status");
  }

  public int getRuns() {
    return runs;
  }

  public double evaulate(int[] toEvaulate) {
    //make a copy of the array to elvaulate,
    int[] nodes = new int[toEvaulate.length];
    System.arraycopy(toEvaulate, 0, nodes, 0, toEvaulate.length);

    double cost = map.getPathDist(nodes);
    if (runBest.getBestCost() == -1.0) //if the global hasn't been set yet then...
    {
      setGlobalBest(nodes, cost);
    } else if (cost < runBest.getBestCost()) //if this cost route is better, (cost lower) then...
    {
      setGlobalBest(nodes, cost);
    }

    return cost;
  }

  public void setGlobalBest(int[] newBest, double newCost) {
    runBest.setGlobalBest(newBest, newCost);

    setChanged();
    notifyObservers("bestupdated");
  }

  public double[][] preCalc(int[] oneOfEachExample) {
    int mapSize = getMap().getSize();
    double[][] allDistances = new double[mapSize][mapSize];

    for (int xCity = 0; xCity < mapSize; xCity++) {
      for (int yCity = 0; yCity < mapSize; yCity++) {
        allDistances[xCity][yCity] = getMap().getDist(oneOfEachExample, xCity, yCity);
        //System.out.println("Pre-Calc'ed: x" + xCity + "  y" + yCity + "    as: " + allDistances[xCity][yCity]);
      }
    }
    return allDistances;
  }
}
