package tspnow.solvers;

import tspnow.CityMap;
import tspnow.Best;
import tspnow.Solver;

/**
 * Solves the TSP problem using a Nearest Neighbor search
 *
 * @author Paul Moggridge
 * @version 4.0.0.1 se
 */
public class NearSolve extends Solver implements Runnable {

  private int[] solution;             //the solution we will modify and keep changing
  private final int[] oneOfEach;
  private int mapSize;

  public NearSolve(int id, CityMap map, int runs, Best runBest) {
    super(id, map, map.getSize(), runBest);
    mapSize = map.getSize();
    //build array each one of each to not be changed and used as starting point each cycle
    oneOfEach = new int[mapSize];
    for (int startBuild = 0; startBuild < mapSize; startBuild++) {
      oneOfEach[startBuild] = startBuild;
    }
  }

  public void run() {
    //pre-calculate all possible connections
    double[][] allDistances = super.preCalc(oneOfEach);               //return an double[c1][c2] array for double distances
    for (int startCity = 0; startCity < mapSize; startCity++) //for each possible starting place
    {
      int[] solution = new int[mapSize];                      //to hold our complete solution
      //.out.println("Starting a city: " + startCity);
      int[] unUsedCities = new int[mapSize];
      System.arraycopy(oneOfEach, 0, unUsedCities, 0, mapSize);

      int lastCity = startCity;
      unUsedCities[startCity] = -42;                                          //mark the city as used
      solution[0] = startCity;

      for (int untilComplete = 1; untilComplete < mapSize; untilComplete++) // populate the solution until it's full
      {
        super.setProgress(startCity);               //update the progress value
        //System.out.println("Filling out: " + untilComplete);
        int nextNearestCity = -2;
        for (int nearSearch = 0; nearSearch < mapSize; nearSearch++) //for all possible cities to visit
        {
          //System.out.println("Searching for nearest city: " + nearSearch);
          if (unUsedCities[nearSearch] != -42) //if it hasn't been used
          {
            if (nextNearestCity == -2) //if unset just set it
            {
              nextNearestCity = nearSearch;
            } else if (allDistances[lastCity][nearSearch] < allDistances[lastCity][nextNearestCity]) // if it's shorter then set it
            {
              nextNearestCity = nearSearch;
            }
          }

        }
        //System.out.println("Next Nearest: " + nextNearestCity);
        //System.out.println("unUsedCities looks like: ");
        //for(int i = 0; i < mapSize; i++)
        //{
        //System.out.println(unUsedCities[i]);
        //}
        //System.out.println("oneOfEach looks like: ");
        //for(int i = 0; i < mapSize; i++)
        //{
        //   System.out.println(oneOfEach[i]);
        //}

        solution[untilComplete] = nextNearestCity;
        lastCity = nextNearestCity;
        unUsedCities[nextNearestCity] = -42;              //mark the city as used
      }

      super.evaulate(solution);     //check result for this run
    }
  }
}
