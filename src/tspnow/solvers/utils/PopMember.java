package tspnow.solvers.utils;

/**
 * Holds a member of population (for the genetic algorithm) along with it's fitness
 *
 * Overrides comparable's compareTo for use with array.sort in genetic algorithm
 *
 * @author Paul Moggridge
 * @version 4.0.0.1 se
 */
public class PopMember implements Comparable<PopMember> {
  // instance variables - replace the example below with your own

  private Double fitness;
  private int[] dna;

  /**
   * Constructor for objects of class PopMember
   */
  public PopMember() {
  }

  public int[] getDNA() {
    return dna;
  }

  public void setDNA(int[] dna) {
    this.dna = dna;
  }

  public double getfitness() {
    return fitness;
  }

  public void setFitness(Double fitness) {
    this.fitness = fitness;
  }

  /**
   * To enable sorting on fitness
   */
  @Override
  public int compareTo(PopMember popMem) {
    return fitness.compareTo(popMem.fitness);
  }
}
