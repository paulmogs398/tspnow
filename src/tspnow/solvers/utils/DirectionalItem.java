package tspnow.solvers.utils;

/**
 * Represent an in item with it's attached direction from the Johnson Trotter Algorithm
 *
 * Based on tutorial: http://programminggeeks.com/java-code-for-permutation-using-steinhaus%E2%80%93johnson%E2%80%93trotter-algorithm/
 *
 * @author Paul Moggridge
 * @version 0.0.0.1
 */
public class DirectionalItem {

  private final int item;
  private static int startId = 0;
  private int id;

  private int dir; // can be -1 or 1 depending on the position

  public DirectionalItem(int item, int dir) {
    this.id = startId++;
    this.item = item;
    this.dir = dir;
  }

  public boolean idIsLower(DirectionalItem dirItem) {
    return (dirItem.getId() > this.id);
  }

  public void reverseDir() {
    this.dir = dir * (-1);
  }

  public int getId() {
    return id;
  }

  public int getEntity() {
    return item;
  }

  public int getDir() {
    return dir;
  }
}
