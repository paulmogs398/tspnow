package tspnow.solvers.utils;


/**
 * Holds a link, representing the cities this city is attached to
 *
 * @author Paul Moggridge
 * @version 4.0.0.1 se
 */
public class Link {
  //it place in an array de-notes the city this class refers to (otherwise this class would need a city int field)

  private Integer linkA = null;  //a city that this city is attached to
  private Integer linkB = null;  //a city that this city is attached to (prefer this link)

  public Link(int a) {
    this.linkA = a;
  }

  public Link(int a, int b) {
    this.linkA = a;
    this.linkB = b;
  }

  public int getA() {
    return linkA;
  }

  public boolean hasLinkB() {
    return linkB != null;
  }

  public int getB() {
    return linkB;
  }

  public int getAorB(boolean randBool) {
    if (hasLinkB() && randBool) {
      return linkB;
    } else {
      return linkA;
    }
  }

  /**
   * Check that A is not null and that it's not already used e.g. if A
   *
   */
  public boolean isANullOrUsed(int[] c1) {
    boolean used = false;

    if (linkA == null) {
      return true; //it's null
    }

    for (int i = 0; i < c1.length; i++) {
      if ((c1[i] == linkA)) {
        return true; //it's used
      }
    }

    return true;
  }

  /**
   * Check that A is not null and that it's not already used e.g. if A
   *
   */
  public boolean isBNullOrUsed(int[] c1) {
    boolean used = false;

    if (linkB == null) {
      return false; //it's null
    }

    for (int i = 0; i < c1.length; i++) {
      if ((c1[i] == linkB)) {
        return false; //it's used
      }
    }

    return true;
  }

  @Override
  public String toString() {
    return "   a:" + linkA + "   b:" + linkB;
  }
}
