package tspnow.solvers;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import tspnow.CityMap;
import tspnow.Best;
import tspnow.Solver;
import tspnow.solvers.utils.DirectionalItem;

/**
 * Solves the TSP problem using a Brute Force Search
 *
 * @author Paul Moggridge
 * @version 4.0.0.1 se
 */
public class BruteSolve extends Solver implements Runnable {

  private int[] solution;             //the solution we will modify and keep changing
  private int mapSize;

  private List<DirectionalItem> entityList = new ArrayList<DirectionalItem>();
  private BigInteger numPermutations = new BigInteger("0");
  private ArrayList<DirectionalItem> sortedList = new ArrayList<DirectionalItem>();
  private int startPosition;

  public BruteSolve(int id, CityMap map, int startPosition, int runs, Best runBest) {
    super(id, map, runs, runBest);
    mapSize = map.getSize();
    this.startPosition = startPosition;

    //build array each one of each
    solution = new int[mapSize];
    for (int startBuild = 0; startBuild < mapSize; startBuild++) {
      solution[startBuild] = startBuild;
    }

    for (int item : solution) {
      DirectionalItem entity = new DirectionalItem(item, -1);
      entityList.add(entity);
    }

    sortedList.addAll(entityList);
    Collections.reverse(sortedList);

    //1st Permutation as is.
    numPermutations = numPermutations.add(BigInteger.ONE);
  }

  /**
   * Gets the mobile directional number by checking the numbers in decreasing
   * order of magnitude
   *
   * @return mobileDirectionalNumber, if it is exist, else null
   */
  private DirectionalItem getMaxMobileDirectionalEntity() {
    for (DirectionalItem dirEntity : sortedList) {
      DirectionalItem maxDirEntity = dirEntity;

      // Now check for the position of maxDirNum in original List
      // to see if this maxDirNum is actually a mobile directional number
      int positionInOriginalList = entityList.indexOf(maxDirEntity);
      if ((maxDirEntity.getDir() == -1 && (positionInOriginalList != 0 && entityList.get(positionInOriginalList - 1).idIsLower(maxDirEntity))
          || (maxDirEntity.getDir() == 1 && (positionInOriginalList != entityList.size() - 1 && entityList.get(positionInOriginalList + 1).idIsLower(maxDirEntity))))) {
        // This is the max mobile directional number - return this
        return maxDirEntity;
      }
    }
    return null; // No more mobile directional numbers exist. All the permutations have been found.
  }

  public void run() {
    super.evaulate(solution);     //check result for this run
    super.setProgress(numPermutations.intValue());  //update the progress value

    for (int r = 0; r < getRuns(); r++) {
      DirectionalItem mobileDirectionalEntity = getMaxMobileDirectionalEntity();
      if (mobileDirectionalEntity == null) {
        // all permutations have been found
        //System.out.println("Total permutations: "+ numPermutations);
        break;
      }
      if (mobileDirectionalEntity.getDir() == 1) {
        // Swap this with the right element in the list
        int positionInOriginalList = entityList.indexOf(mobileDirectionalEntity);
        entityList.set(positionInOriginalList, entityList.get(positionInOriginalList + 1));
        entityList.set(positionInOriginalList + 1, mobileDirectionalEntity);
      } else {
        // Swap this with the left element in the list
        int positionInOriginalList = entityList.indexOf(mobileDirectionalEntity);
        entityList.set(positionInOriginalList, entityList.get(positionInOriginalList - 1));
        entityList.set(positionInOriginalList - 1, mobileDirectionalEntity);
      }

      if (r >= startPosition) {
        for (int e = 0; e < entityList.size(); e++) {
          solution[e] = entityList.get(e).getEntity();
        }

        super.evaulate(solution);     //check result for this run
        super.setProgress(numPermutations.intValue());  //update the progress   value
      }

      //System.out.println("");
      numPermutations = numPermutations.add(BigInteger.ONE);

      // Now reverse the direction of all the numbers greater than mobileDirectionalNumber
      for (DirectionalItem e : entityList) {
        if (mobileDirectionalEntity.idIsLower(e)) //if the id is smaller than
        {
          e.reverseDir();
        }
      }
    }
  }
}
