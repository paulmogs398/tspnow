package tspnow.solvers;

import java.util.Random;
import java.util.Arrays;

import tspnow.CityMap;
import tspnow.Best;
import tspnow.Solver;
import tspnow.solvers.utils.PopMember;
import tspnow.solvers.utils.Link;

/**
 * Solves the TSP problem using a crude/convential Genetic Algorithm
 *
 * Inspired by: http://www.lalena.com/ai/tsp/
 * https://www.youtube.com/watch?v=ejxfTy4lI6I
 *
 * @author Paul Moggridge
 * @version 4.0.0.1 "se"
 */
public class GeneticSolve extends Solver implements Runnable {

  private final int popSize;                      //the size of the population
  private final int crossOvers;                   //the number of pairs to crossover e.g. 1 would cause one couple to cross over
  private final int mutations;                    //the number random mutation to occur in the population
  private Random rand;                            //the random generator for creating the initial population and causing the mutations
  private int[][] population;                     //the solutions that will be breed, killed and mutated find optimial solution
  private PopMember[] populationSortable;         //the same as the above but just used for sorting
  private double[][] allDistances;                //distances between all the nodes in the citymap  (required for when cross over has to nearest neighbour)

  public GeneticSolve(int id, CityMap map, int popSize, int crossOvers, int mutations, long randomSeed, int runs, Best runBest){
    super(id, map, runs, runBest);

    this.popSize = popSize;
    this.crossOvers = crossOvers;
    this.mutations = mutations;
    rand = new Random(randomSeed);

    // Overwrite crossover settings if crossover is more than the pop size / 2
    if (map.getSize() < 3 || crossOvers > (map.getSize() / 2) || popSize < 3) {
      crossOvers = ((map.getSize() / 2)-1);
    }

    //init population
    population = new int[popSize][map.getSize()];
    populationSortable = new PopMember[popSize];

    //create a init solution
    int[] oneOfEach = new int[map.getSize()];
    for (int startBuild = 0; startBuild < map.getSize(); startBuild++) {
      oneOfEach[startBuild] = startBuild;
    }
    allDistances = super.preCalc(oneOfEach); //return an double[c1][c2] array for double distances, nessary for when crossOver has no usable links

    //create a population of the init solution
    for (int initPop = 0; initPop < popSize; initPop++) {
      population[initPop] = oneOfEach.clone();
      populationSortable[initPop] = new PopMember();
    }

    //scrabble up the genes of the init population so there some initial variation
    int swapOne;
    int swapTwo;
    int swapTemp;
    for (int initPop = 0; initPop < popSize; initPop++) {
      for (int swaps = 0; swaps < map.getSize(); swaps++) {
        swapOne = rand.nextInt(map.getSize());     //Set Swap Location One
        swapTwo = rand.nextInt(map.getSize());     //Set Swap Location Two
        //swap over
        swapTemp = population[initPop][swapOne];
        population[initPop][swapOne] = population[initPop][swapTwo];
        population[initPop][swapTwo] = swapTemp;

      }
    }

    //print the population
    //printPopulation();
    //Link[] test = getLinks(population[0]);
    //for(Link l : test)
    //{
    //     System.out.println("city: " + l);
    //}
    //evaulatePopulation();
    //randomMutations();
    //printPopulation();
  }

  public void run() {
    for (int run = 0; run < getRuns(); run++) {
      evaulatePopulation();
      crossOver();
      randomMutations();
      super.setProgress(run);  //update the progress value
    }
  }

  /**
   * Evaualtes and sorts the population by fitness, (best to lowest)
   */
  private void evaulatePopulation() {
    for (int p = 0; p < popSize; p++) {
      //place into sortable array
      populationSortable[p].setFitness(super.evaulate(population[p])); //evaulate
      populationSortable[p].setDNA(population[p]);
    }
    //sort
    Arrays.sort(populationSortable);
    for (int p = 0; p < popSize; p++) {
      //place back into population now in order
      population[p] = populationSortable[p].getDNA();
    }
  }

  /**
   * Crosses over the best entries (assumes the list is sorted on fitness)
   */
  private void crossOver() {
    for (int ip = 0; ip < crossOvers; ip++) {
      //get the fittest
      int[] p0 = population[ip];
      int[] p1 = population[ip + 1];

      //init there list of links
      Link[] pl0 = new Link[p0.length];
      Link[] pl1 = new Link[p1.length];
      pl0 = getLinks(p0);
      pl1 = getLinks(p1);
      Link[][] parents = {pl0, pl1};

      //pick a parent at random to start with
      int parentToggle = rand.nextInt(2); //to use to alternate between the parents, 0 means p0, 1 means p1

      //create the child
      int[] c1 = new int[p0.length];

      //alternate the parent
      if (parentToggle == 0) {
        c1[0] = p0[0];
        parentToggle = 1;
      } else {
        c1[0] = p1[0];
        parentToggle = 0;
      }

      //populate the remaining
      for (int c = 1; c < c1.length; c++) //cycle through  1 - end of the childs dna
      {
        if (parents[parentToggle][c1[c - 1]].isBNullOrUsed(c1)) //get the what the parent links the latest entry in child dna to and check it has an A or B that is suitable
        {
          c1[c] = parents[parentToggle][c1[c - 1]].getB();
          continue;
        } else if (parents[parentToggle][c1[c - 1]].isANullOrUsed(c1)) {
          c1[c] = parents[parentToggle][c1[c - 1]].getA();
          continue;
        } else //toggle the list to look at
        if (parentToggle == 0) {
          c1[0] = p0[0];
          parentToggle = 1;
        } else {
          c1[0] = p1[0];
          parentToggle = 0;
        }

        if (parents[parentToggle][c1[c - 1]].isANullOrUsed(c1)) //check the first alternated to parent
        {
          c1[c] = parents[parentToggle][c1[c - 1]].getAorB(rand.nextBoolean());
          continue;
        }
        if (parents[parentToggle][c1[c - 1]].isBNullOrUsed(c1)) //check the first alternated to parent
        {
          c1[c] = parents[parentToggle][c1[c - 1]].getAorB(rand.nextBoolean());
          continue;
        } else //nearest neighbour it
        {
          c1[c] = nearestNeighbour(c1);
        }
      }
    }
  }

  private Link[] getLinks(int[] parent) {
    Link[] toReturn = new Link[parent.length];
    for (int p = 0; p < parent.length; p++) {
      if (p == 0) {
        //the link is only going to have one as it's at the start
        toReturn[parent[p]] = new Link(parent[p + 1]);
      } else if (p == (parent.length - 1)) {
        //the link is only going to have one as it's at the end
        toReturn[parent[p]] = new Link(parent[p - 1]);
      } else {
        toReturn[parent[p]] = new Link(parent[p - 1], parent[p + 1]);
      }
    }
    return toReturn;
  }

  /**
   * Causes random mutations in the population
   */
  private void randomMutations() {
    int swapOne;
    int swapTwo;
    int swapTemp;
    for (int m = 0; m < mutations; m++) {
      int toMutate = rand.nextInt(popSize);
      swapOne = rand.nextInt(getMap().getSize());     //Set Swap Location One
      swapTwo = rand.nextInt(getMap().getSize());     //Set Swap Location Two
      //swap over
      swapTemp = population[toMutate][swapOne];
      population[toMutate][swapOne] = population[toMutate][swapTwo];
      population[toMutate][swapTwo] = swapTemp;
    }
  }

  private void printPopulation() {
    System.out.println("#population start");
    for (int pop = 0; pop < popSize; pop++) {
      for (int c = 0; c < population[pop].length; c++) {
        System.out.print(population[pop][c]);
      }
      System.out.println(" - " + population[pop]);
    }
    System.out.println("#population end");
  }

  private int nearestNeighbour(int[] childToAppendTo) {
    int nextNearest = -1;

    //loop over the distances from this node the other nodes breaking/ignoring them if they are taken of further than the current best
    for (int n = 0; n < getMap().getSize(); n++) {
      //if it's already used ignore it. (loop over the used cities checking each one
      boolean used = false; //to store whether the loop exited because of city being used or not
      for (int p : childToAppendTo) //for each city in the so far competeted
      {
        if (p == n) //if it's the same as the currentlt being tested city,
        {
          used = true; //mark that it's used
          break;          //excape the loop
        }
      }
      if (used) //did the above loop find it to used?
      {
        continue; //if yes then n the currently being tested city is no use move on
      }

      //if it's shorter than the currently held shortest path, then replace the shortest path
      if (nextNearest == -1 || allDistances[childToAppendTo[childToAppendTo.length - 1]][n] < allDistances[childToAppendTo[childToAppendTo.length - 1]][nextNearest]) {
        nextNearest = n; //set the id of the next nearest city to be return
      }
    }

    return nextNearest;
  }
}
