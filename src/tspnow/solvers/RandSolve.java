package tspnow.solvers;

import java.util.Random;

import tspnow.CityMap;
import tspnow.Best;
import tspnow.Solver;

/**
 * Solves the TSP problem using a Random Walk Search
 *
 * @author Paul Moggridge
 * @version 4.0.0.1 se
 */
public class RandSolve extends Solver implements Runnable {

  private final Random rand;                // generates random numbers to build up the solutions
  private final int mapSize;                // the size of the map
  private final int[] oneOfEach;      //array to swap around that includes only one of each int city
  private int[] solution;             //the solution we will modify and keep changing
  private int swapOne;                //a location to switch with
  private int swapTwo;                //a location to switch with
  private int swapTemp;               //Temporary memory to help the switch

  //                super    super       subthis
  public RandSolve(int id, CityMap map, long seed, int runs, Best runBest) {
    super(id, map, runs, runBest);   //pass up the solver the id and citymap
    mapSize = map.getSize();      //get the map size,
    solution = new int[mapSize];  //ready the solution array
    rand = new Random(seed);     //set up the random number generator

    //build array each one of each to not be changed and used as starting point each cycle
    oneOfEach = new int[mapSize];
    for (int startBuild = 0; startBuild < mapSize; startBuild++) {
      oneOfEach[startBuild] = startBuild;
    }
  }

  public void run() {
    for (int runCount = 1; runCount <= getRuns(); runCount++) {
      solution = oneOfEach;
      for (int swaps = 0; swaps < mapSize; swaps++) {
        swapOne = rand.nextInt(mapSize);     //Set Swap Location One
        swapTwo = rand.nextInt(mapSize);     //Set Swap Location Two
        //swap over
        swapTemp = solution[swapOne];
        solution[swapOne] = solution[swapTwo];
        solution[swapTwo] = swapTemp;

      }
      super.evaulate(solution);     //check result for this run
      super.setProgress(runCount);  //update the progress value
    }
  }
}
