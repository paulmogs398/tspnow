package tspnow;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.ImageIcon;
import javax.swing.JTextField;
import javax.swing.JProgressBar;
import java.util.Observer;
import java.util.Observable;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Clipboard;
import java.awt.Toolkit;
import javax.swing.JPopupMenu;
import javax.swing.JDialog;
import javax.swing.JCheckBox;

//handling the special xml reading exceptions
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import org.xml.sax.*; //abstract Document class
import java.io.IOException;

/**
 * Graphical User Interface and Visualiser for TSPnowX
 *
 * @author Paul Moggridge
 * @version "4.0.0.1" se
 */
public class Gui extends Observable implements Runnable {

  private TSPnow tsp;

  private final JFrame mainWindow = new JFrame("TSPnowX");
  private Draw d;

  //filechooser
  private JFileChooser fileChooser = new JFileChooser();

  //popup menu on the draw
  private JPopupMenu drawMenu = new JPopupMenu();
  private JMenuItem AddCityHereItem = new JMenuItem("Add City Here");
  private JMenuItem refreshPopItem = new JMenuItem("Refresh the Map");

  //the menu across the top of the window
  private final JMenuBar menubar = new JMenuBar();
  //general headings
  private final JMenu buildDrop = new JMenu("Build");
  private final JMenu runDrop = new JMenu("Run");
  private final JMenu viewDrop = new JMenu("View");
  private final JMenu toolsDrop = new JMenu("Tools");
  //build
  private JMenuItem buildItem = new JMenuItem("Build City");
  private JMenuItem randBuildItem = new JMenuItem("Random Build Cities");
  private JMenuItem openItem = new JMenuItem("Open Map");
  private JMenuItem importItem = new JMenuItem("Import CSV Map");
  private JMenuItem saveItem = new JMenuItem("Save Map");
  //run menu
  private JMenuItem randItem = new JMenuItem("Random Solve");
  private JMenuItem nearItem = new JMenuItem("Nearest Solve");
  private JMenuItem bruteItem = new JMenuItem("Brute Force Solve");
  private JMenuItem gaItem = new JMenuItem("Genetic Algorithm Solve");
  //tools
  private JMenuItem resetItem = new JMenuItem("Reset");
  private JMenuItem exitItem = new JMenuItem("Exit");
  //view
  private JMenuItem flashGuiItem = new JMenuItem("Update GUI");  //flash once
  private JMenuItem colourSettingsItem = new JMenuItem("Colour Settings");
  private JCheckBox drawNumbers = new JCheckBox("Show City ID's", true);

  //dialog windows
  private JColorChooser pickColour = new JColorChooser();
  private JDialog buildCity = new JDialog(mainWindow, "Build a City", true);
  private JTextField buildCityXCor = new JTextField();
  private JTextField buildCityYCor = new JTextField();
  private JButton buildCityOk = new JButton("Ok");
  private JDialog randCity = new JDialog(mainWindow, "Randomly Build Cities", true);
  private JTextField randCitySeed = new JTextField();
  private JTextField randCityQuantity = new JTextField();
  private JButton randCityOk = new JButton("Ok");
  private JDialog randDialog = new JDialog(mainWindow, "Rand Solve", true);
  private JTextField randDialogSeed = new JTextField();
  private JTextField randDialogRuns = new JTextField();
  private JButton randDialogOk = new JButton("Ok");
  private JButton randDialogCancel = new JButton("Cancel");
  private JDialog gaDialog = new JDialog(mainWindow, "Genetic Algorithm Solve", true);
  private JTextField gaDialogSeed = new JTextField();
  private JTextField gaDialogRuns = new JTextField();
  private JTextField gaDialogPopSize = new JTextField();
  private JTextField gaDialogCrossOvers = new JTextField();
  private JTextField gaDialogMutations = new JTextField();
  private JButton gaDialogOk = new JButton("Ok");
  private JButton gaDialogCancel = new JButton("Cancel");
  //status area
  private JPanel statusArea = new JPanel();
  private JPanel bestArea = new JPanel();
  private JPanel loadArea = new JPanel();
  private JPanel bestButtonArea = new JPanel();
  private JLabel threadsDisplay = new JLabel("Active Solver Threads:   0 ");
  private JProgressBar percentDisplay = new JProgressBar(0, 100);
  private JLabel bestText = new JLabel("Best Solution:");
  private JLabel bestDisplay = new JLabel("");
  private JLabel costText = new JLabel("Best Solution Cost: ");
  private JLabel costDisplay = new JLabel("-1.0");
  private JButton refresh = new JButton(new ImageIcon("res/images/refresh.png"));
  private JButton clearBest = new JButton(new ImageIcon("res/images/clear.png"));
  private JButton copyBest = new JButton(new ImageIcon("res/images/copy.png"));

  //event handling for updating the gui
  UpdateEvent fe = new UpdateEvent(); //flash events events that involve flashing the gui

  /**
   * Constructor for objects of class Gui
   */
  public Gui() {
    tsp = new TSPnow();
    (tsp).addObserver(fe); //add observer to allow the gui to know when you update
    mainWindow.setSize(800, 600);
    mainWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    mainWindow.setIconImage(new ImageIcon("res/images/icon.png").getImage());
    mainWindow.setLayout(new BorderLayout());
    d = new Draw(tsp.map, tsp.getBest().getSeq(), Color.RED, Color.GREEN, Color.BLACK);
    mainWindow.add(d, BorderLayout.CENTER);

    //build menu
    buildDrop.add(buildItem);
    buildDrop.add(randBuildItem);
    buildDrop.addSeparator();
    buildDrop.add(openItem);
    buildDrop.add(importItem);
    buildDrop.add(saveItem);
    buildItem.addActionListener(new actionEvent());
    randBuildItem.addActionListener(new actionEvent());
    openItem.addActionListener(new actionEvent());
    importItem.addActionListener(new actionEvent());
    saveItem.addActionListener(new actionEvent());
    menubar.add(buildDrop);
    //run menu
    runDrop.add(randItem);
    runDrop.add(nearItem);
    runDrop.add(bruteItem);
    runDrop.add(gaItem);
    randItem.addActionListener(new actionEvent());
    nearItem.addActionListener(new actionEvent());
    bruteItem.addActionListener(new actionEvent());
    gaItem.addActionListener(new actionEvent());
    menubar.add(runDrop);
    //tools menu
    toolsDrop.add(resetItem);
    toolsDrop.add(exitItem);
    resetItem.addActionListener(new actionEvent());
    exitItem.addActionListener(new actionEvent());
    menubar.add(toolsDrop);
    //view menu
    viewDrop.add(flashGuiItem);
    viewDrop.add(colourSettingsItem);
    viewDrop.add(drawNumbers);
    flashGuiItem.addActionListener(new actionEvent());
    colourSettingsItem.addActionListener(new actionEvent());
    drawNumbers.addActionListener(new actionEvent());
    menubar.add(viewDrop);
    //set menu bar ontop of the main window
    mainWindow.setJMenuBar(menubar);
    //setup the status area
    statusArea.setLayout(new GridLayout(2, 1));
    loadArea.setLayout(new BorderLayout());
    bestArea.setLayout(new GridLayout(1, 5));
    bestButtonArea.setLayout(new GridLayout(1, 3));
    loadArea.add(threadsDisplay, BorderLayout.LINE_START);
    percentDisplay.setSize(650, 7);
    percentDisplay.setValue(100);
    percentDisplay.setStringPainted(true);
    loadArea.add(percentDisplay, BorderLayout.CENTER);
    bestArea.add(bestText);
    bestArea.add(bestDisplay);
    bestArea.add(costText);
    bestArea.add(costDisplay);
    refresh.setToolTipText("Refresh this status stuff.");
    bestButtonArea.add(refresh);
    refresh.addActionListener(new actionEvent());
    clearBest.setToolTipText("Erase the current best solution.");
    bestButtonArea.add(clearBest);
    clearBest.addActionListener(new actionEvent());
    copyBest.setToolTipText("Copy the best solution and it's cost to the clipboard.");
    bestButtonArea.add(copyBest);
    copyBest.addActionListener(new actionEvent());
    bestArea.add(bestButtonArea);
    statusArea.add(loadArea);
    statusArea.add(bestArea);
    mainWindow.add(statusArea, BorderLayout.PAGE_START);

    // Draw pop up menu
    //
    //
    drawMenu.add(AddCityHereItem);
    AddCityHereItem.addActionListener(new actionEvent());
    drawMenu.add(refreshPopItem);
    refreshPopItem.addActionListener(new actionEvent());
    d.setComponentPopupMenu(drawMenu);

    //   Dialog windows
    //
    //   Build a city
    buildCity.setSize(200, 100);
    buildCity.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    buildCity.setLayout(new GridLayout(3, 2));
    buildCity.add(new JLabel("X Cor:"));
    buildCity.add(buildCityXCor);
    buildCity.add(new JLabel("Y Cor:"));
    buildCity.add(buildCityYCor);
    buildCity.add(buildCityOk);
    buildCityOk.addActionListener(new actionEvent());
    //random build cities
    randCity.setSize(200, 100);
    randCity.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    randCity.setLayout(new GridLayout(3, 2));
    randCity.add(new JLabel("Seed:"));
    randCity.add(randCitySeed);
    randCity.add(new JLabel("Quantity:"));
    randCity.add(randCityQuantity);
    randCity.add(randCityOk);
    randCityOk.addActionListener(new actionEvent());
    //randSolve
    randDialog.setSize(200, 100);
    randDialog.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    randDialog.setLayout(new GridLayout(3, 2));
    randDialog.add(new JLabel("Seed:"));
    randDialog.add(randDialogSeed);
    randDialog.add(new JLabel("Runs:"));
    randDialog.add(randDialogRuns);
    randDialog.add(randDialogOk);
    randDialogOk.addActionListener(new actionEvent());
    randDialog.add(randDialogCancel);
    randDialogCancel.addActionListener(new actionEvent());
    //ga solve dialog
    gaDialog.setSize(300, 150);
    gaDialog.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    gaDialog.setLayout(new GridLayout(6, 2));
    gaDialog.add(new JLabel("Seed:"));
    gaDialog.add(gaDialogSeed);
    gaDialog.add(new JLabel("Runs:"));
    gaDialog.add(gaDialogRuns);
    gaDialog.add(new JLabel("Population Size:"));
    gaDialog.add(gaDialogPopSize);
    gaDialog.add(new JLabel("Cross-Overs:"));
    gaDialog.add(gaDialogCrossOvers);
    gaDialog.add(new JLabel("Mutation:"));
    gaDialog.add(gaDialogMutations);
    gaDialog.add(gaDialogOk);
    gaDialogOk.addActionListener(new actionEvent());
    gaDialog.add(gaDialogCancel);
    gaDialogCancel.addActionListener(new actionEvent());

    mainWindow.setVisible(true);
  }

  public void run() {
  }

  ;  //nessary for EDT technology

    /**
     * An action listener for responsing to button presses
     */
    public class actionEvent implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) //TODO maybe enumerate this
    {
      //drop down menu options
      if (e.getSource() == buildItem) {
        buildItem();
      }
      if (e.getSource() == randBuildItem) {
        randBuildItem();
      }
      if (e.getSource() == openItem) {
        openItem();
      }
      if (e.getSource() == importItem) {
        importItem();
      }
      if (e.getSource() == saveItem) {
        saveItem();
      }
      if (e.getSource() == randItem) {
        randItem();
      }
      if (e.getSource() == nearItem) {
        nearItem();
      }
      if (e.getSource() == bruteItem) {
        bruteItem();
      }
      if (e.getSource() == gaItem) {
        gaItem();
      }
      if (e.getSource() == resetItem) {
        resetItem();
      }
      if (e.getSource() == exitItem) {
        exitItem();
      }
      if (e.getSource() == colourSettingsItem) {
        colourSettingsItem();
      }
      if (e.getSource() == flashGuiItem) {
        updateGui();
      }
      if (e.getSource() == drawNumbers) {
        showHideNumbers();
      }
      //build stuff
      if (e.getSource() == buildCityOk) {
        buildCityOk();
      }
      if (e.getSource() == randCityOk) {
        randCityOk();
      }
      //randsolve ok
      if (e.getSource() == randDialogOk) {
        randSolveOk();
      }
      if (e.getSource() == randDialogCancel) {
        randSolveCancel();
      }
      //gaDialog
      if (e.getSource() == gaDialogOk) {
        gaSolveOk();
      }
      if (e.getSource() == gaDialogCancel) {
        gaSolveCancel();
      }
      //status dialog options
      if (e.getSource() == refresh) {
        updateStatus();
      }
      if (e.getSource() == clearBest) {
        clearBest();
      }
      if (e.getSource() == copyBest) {
        copyBest();
      }
      //pop context menu thing on the draw
      if (e.getSource() == AddCityHereItem) {
        addCityHere();
      }
      if (e.getSource() == refreshPopItem) {
        updateGui();
      }
    }
  }

  /**
   * Event handling - Observer pattern
   */
  public class UpdateEvent implements Observer {

    @Override
    public void update(Observable obj, Object arg) {
      if (arg instanceof String) {
        String updateType = (String) arg;
        if (updateType.equals("StatusArea")) {
          //update the status area as the situation below has changed
          updateStatus();
        }
        if (updateType.equals("MapArea")) {
          //update the status area as the situation below has changed
          updateMap();
        }
      }
    }
  }

  //#################################################
  //Updating the User Interface
  //#################################################
  /**
   * Updates both the Status and Map areas
   */
  private void updateGui() {
    updateStatus();
    updateMap();
  }

  /**
   * Updates the status area
   */
  private void updateStatus() {
    threadsDisplay.setText("Active Solver Threads:   " + tsp.getNumberRunningThreads() + " ");
    //System.out.println((int)tsp.getPercentComplete());
    percentDisplay.setValue((int) tsp.getPercentComplete());
    String bestSolution = "No solution ...yet";
    int bestSize = 0;
    if (tsp.getBest() != null && tsp.getBest().getSeq() != null) {
      bestSolution = "";
      bestSize = tsp.getBest().getSeq().length;
    }

    for (int bestPrint = 0; bestPrint < bestSize; bestPrint++) {
      bestSolution = bestSolution + (tsp.getBest().getSeq()[bestPrint] + ".");
    }

    bestDisplay.setText(bestSolution);
    costDisplay.setText("" + tsp.getBest().getBestCost());
    percentDisplay.repaint();
  }

  /**
   * Updates the map area
   */
  private void updateMap() {
    d.updateSolution(tsp.getBest().getSeq());
    d.updateMap(tsp.getMap());
    d.repaint();

    mainWindow.invalidate();
    mainWindow.repaint();
    mainWindow.validate();
  }

  //#################################################
  // Building cities etc
  //#################################################
  /**
   * Open the build a city menu
   */
  private void buildItem() {
    buildCity.setVisible(true);
  }

  /**
   * Calls method to build city after getting text from the dialog
   */
  private void buildCityOk() {
    System.out.println("building...");
    tsp.addCity(buildCityXCor.getText(), buildCityYCor.getText());
    buildCity.setVisible(false);
  }

  /**
   * Open the random city builder
   */
  private void randBuildItem() {
    randCity.setVisible(true);
  }

  /**
   * On ress of OK create the random cities
   */
  private void randCityOk() {
    try {
      tsp.randCity(d.getSize().getWidth() + "", d.getSize().getHeight() + "", randCityQuantity.getText(), randCitySeed.getText());
    } catch (NumberFormatException nfe) {
      JOptionPane.showMessageDialog(mainWindow, "Failed to random create cities " + nfe.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
    } catch (TSPnow.TSPnowInputException tspie) {
      JOptionPane.showMessageDialog(mainWindow, "Failed to random create cities " + tspie.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
    }
    randCity.setVisible(false);
  }

  /**
   * build city where the mouse is
   */
  private void addCityHere() {
    //get the mouse cors
    if (d.getMousePosition() != null && d.getMousePosition().getX() > 20 && d.getMousePosition().getY() > 20) {
      int xCor = (int) Math.round(d.getMousePosition().getX());
      int yCor = (int) Math.round(d.getMousePosition().getY());
      tsp.addCity(xCor - 20, yCor - 20);
    }
  }

  /**
   * Opens the open dialog
   */
  private void openItem() {
    int fileChooserResult = fileChooser.showOpenDialog(mainWindow);
    if (fileChooserResult == JFileChooser.APPROVE_OPTION) {
      File file = fileChooser.getSelectedFile();
      try {
        tsp.openFile(file.getPath());
      } catch (ParserConfigurationException pce) {
        JOptionPane.showMessageDialog(mainWindow, "Problems reading the map file: " + pce.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
      } catch (IOException ioe) {
        JOptionPane.showMessageDialog(mainWindow, "Problems reading the map file: " + ioe.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
      } catch (SAXException saxe) {
        JOptionPane.showMessageDialog(mainWindow, "Problems reading the map file: " + saxe.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
      } catch (Exception e) {
        JOptionPane.showMessageDialog(mainWindow, "Problems reading the map file: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
      }
    } else {
      JOptionPane.showMessageDialog(mainWindow, "Could not open map file.", "Error", JOptionPane.ERROR_MESSAGE);
    }
  }

  /**
   * Opens the import dialog
   */
  private void importItem() {
    int fileChooserResult = fileChooser.showOpenDialog(mainWindow);
    if (fileChooserResult == JFileChooser.APPROVE_OPTION) {
      File file = fileChooser.getSelectedFile();
      try {
        tsp.importCSV(file.getPath());
      } catch (ParserConfigurationException pce) {
        JOptionPane.showMessageDialog(mainWindow, "Problems writing the map file: " + pce.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
      } catch (IOException ioe) {
        JOptionPane.showMessageDialog(mainWindow, "Problems writing the map file: " + ioe.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
      } catch (SAXException saxe) {
        JOptionPane.showMessageDialog(mainWindow, "Problems writing the map file: " + saxe.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
      } catch (Exception e) {
        JOptionPane.showMessageDialog(mainWindow, "Problems writing the map file: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
      }
    } else {
      JOptionPane.showMessageDialog(mainWindow, "Could not open map file.", "Error", JOptionPane.ERROR_MESSAGE);
    }
  }

  /**
   * Opens the save dialog
   */
  private void saveItem() {
    int fileChooserResult = fileChooser.showOpenDialog(mainWindow);
    if (fileChooserResult == JFileChooser.APPROVE_OPTION) {
      File file = fileChooser.getSelectedFile();
      try {
        tsp.saveFile(file.getPath());
      } catch (IOException ioe) {
        JOptionPane.showMessageDialog(mainWindow, "Problems writing the map file: " + ioe.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
      } catch (TransformerException te) {
        JOptionPane.showMessageDialog(mainWindow, "Problems writing the map file: " + te.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
      } catch (ParserConfigurationException pce) {
        JOptionPane.showMessageDialog(mainWindow, "Problems writing the map file: " + pce.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
      }
    } else {
      JOptionPane.showMessageDialog(mainWindow, "Could not save map file.", "Error", JOptionPane.ERROR_MESSAGE);
    }
  }

  //#################################################
  // The Solvers
  //#################################################
  /**
   * Opens the random solver dialog
   */
  private void randItem() {
    randDialog.setVisible(true);
  }

  /**
   * Launches the random Solve
   */
  private void randSolveOk() {
    long seed;
    int runs;
    try {
      seed = Long.parseLong(randDialogSeed.getText());
      runs = Integer.parseInt(randDialogRuns.getText());
    } catch (NumberFormatException nfe) {
      JOptionPane.showMessageDialog(mainWindow, "Numbers only please.", "Error", JOptionPane.ERROR_MESSAGE);
      randDialog.setVisible(false);
      return;
    }

    tsp.randSolve(seed, runs);
    randDialog.setVisible(false);
  }

  /**
   * Close the randSolve dialog
   */
  private void randSolveCancel() {
    randDialog.setVisible(false);
  }

  /**
   * Opens the near neighbour solver dialog
   */
  private void nearItem() {
    tsp.nearSolve();
  }

  /**
   * Opens the brute force solver dialog
   */
  private void bruteItem() {
    tsp.bruteSolve();
  }

  /**
   * Opens the genetic algorithm solve dialog
   */
  private void gaItem() {
    gaDialog.setVisible(true);
  }

  private void gaSolveOk() {
    long seed;
    int runs;
    int popSize;
    int mutations;
    int crossOvers;

    try {
      seed = Long.parseLong(gaDialogSeed.getText());
      runs = Integer.parseInt(gaDialogRuns.getText());
      popSize = Integer.parseInt(gaDialogPopSize.getText());
      mutations = Integer.parseInt(gaDialogMutations.getText());
      crossOvers = Integer.parseInt(gaDialogCrossOvers.getText());
    } catch (NumberFormatException nfe) {
      JOptionPane.showMessageDialog(mainWindow, "Numbers only please.", "Error", JOptionPane.ERROR_MESSAGE);
      gaDialog.setVisible(false);
      return;
    }
    tsp.gaSolve(seed, runs, popSize, crossOvers, mutations);
    gaDialog.setVisible(false);
  }

  private void gaSolveCancel() {
    gaDialog.setVisible(false);
  }

  //########################################
  // All the other stuff ...
  //########################################
  /**
   * Ask the tsp engine to reset
   */
  private void resetItem() {
    tsp.reset();
    clearBest();
    updateStatus();
    updateMap();
  }

  /**
   * Exits TSPnow
   */
  private void exitItem() {
    System.exit(0);
  }

  /**
   * Change the colour of the TSP visualisation!
   */
  private void colourSettingsItem() {
    d.setBackGroundColor(JColorChooser.showDialog(mainWindow, "Choose Map Background Color", Color.BLACK));
    d.setLineColor(JColorChooser.showDialog(mainWindow, "Choose Line Color", Color.RED));
    d.setCityColor(JColorChooser.showDialog(mainWindow, "Choose City Color", Color.GREEN));
  }

  /**
   * Erases the best so far value
   */
  private void clearBest() {
    tsp.clearBest();
    updateMap();
  }

  private void copyBest() {
    StringSelection bestCopyText = new StringSelection(tsp.getBest().toString());
    Clipboard clipb = Toolkit.getDefaultToolkit().getSystemClipboard();
    clipb.setContents(bestCopyText, bestCopyText);
  }

  private void showHideNumbers() {
    if (d.getDrawNumbers() == true) {
      d.setDrawNumbers(false);
      drawNumbers.setSelected(false);
    } else {
      d.setDrawNumbers(true);
      drawNumbers.setSelected(true);
    }
    updateGui();
  }
}
