package tspnow;

/**
 *  Holds the best solution, in a object all solvers can update
 *
 * @author Paul Moggridge
 * @version 0.1 build 2 "gtr"
 */
public class Best {

  private static int[] globalBest = new int[0];    //globalBest route, this is to draw
  private static double globalBestCost = -1;       //the cost of this route
  private CityMap map;

  /**
   * Constructor for objects of class Best
   */
  public Best(CityMap map) {
    this.map = map;
  }

  // The below methods are snychronised
  // such that only one of them can be
  // at any one time.
  /**
   *
   * @return the current best solution as an int[]
   */
  public synchronized int[] getSeq() {
    return globalBest;
  }

  /**
   * Storing this with the solution is done avoid un-nessary re-calculation
   *
   * @return the current best solution's cost
   */
  public synchronized double getBestCost() {
    return globalBestCost;
  }

  /**
   * Sets the best to a new value,
   *
   *
   * @param nodes the new solution to set the best to
   * @param cost the cost of the new solution
   */
  public synchronized void setGlobalBest(int[] nodes, double cost) {
    globalBest = nodes;
    globalBestCost = cost;
  }

  public synchronized void clear() {
    globalBest = new int[0];
    globalBestCost = -1;
  }

  @Override
  public String toString() {
    String nodesToVisit = "";

    for (int nodeId : getSeq()) {
      nodesToVisit = nodesToVisit + nodeId + ",";
    }

    return nodesToVisit + getBestCost();  //e.g. 1,2,3,4,225.1082
  }
}
