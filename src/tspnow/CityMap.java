package tspnow;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

/**
 * Write a description of class CityMap here.
 *
 * @author Paul Moggridge
 * @version "4.0.0.1" se
 */
public class CityMap {

  private City[] map = new City[0];

  public CityMap() {

  }

  public CityMap(City[] newMap) {
    map = newMap;
  }

  /**
   * Returns the City found at the given index. Returns null if given a false
   * index.
   *
   * @param index int to find city by
   * @return City from the request index
   */
  public City getCity(int index) {
    return map[index];
  }

  /**
   * Adds a city to the CityMap, has to use arraycopy and thus isn't that fast
   * to add +1000 but array is so much fast to read it's worth the slight extra
   * cost at load in.
   *
   * @param newCity is the city to add to this city map
   */
  public void addCity(City newCity) {
    City[] newMap = new City[map.length + 1];
    System.arraycopy(map, 0, newMap, 0, map.length);
    newMap[newMap.length - 1] = newCity;
    map = newMap;
  }

  /**
   * Returns the size of this city map
   *
   * @return int of size of this city map
   */
  public int getSize() {
    if (map != null) {
      return map.length;
    } else {
      return 0;
    }
  }

  /**
   * Return the distance cost of a path
   *
   * @return double of the cost of the path
   */
  public double getPathDist(int[] nodes) {
    double cost = -2.0;
    int solutionLength = nodes.length - 2;
    for (int addCosts = 0; addCosts <= solutionLength; addCosts++) {
      cost += getDist(nodes, addCosts, addCosts + 1);
      //System.out.println("computing cost... at pass:" + addCosts + "   cost now at: " + cost);
    }
    return cost;
  }

  /**
   * Return the distance between two nodes, can take a path with the location to
   * check between, so that a new int[] interation is required taking extra
   * time/resources.
   *
   * @param nodes int[] is the path, or two nodes
   * @param a int is node to measure distance from
   * @param b int is node to measure distance too
   * @return double of the cost of the path
   */
  public double getDist(int[] nodes, int a, int b) {
    return sqrt(pow((map[nodes[a]].getX() - map[nodes[b]].getX()), 2) + pow((map[nodes[a]].getY() - map[nodes[b]].getY()), 2));
  }

  public String listNodes() {
    String listOfNodes = "";
    for (int list = 0; list < map.length; list++) {
      listOfNodes = listOfNodes + map[list].getX() + "\n" + map[list].getY() + "\n";
    }
    return listOfNodes;
  }
}
