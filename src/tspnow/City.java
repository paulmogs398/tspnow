package tspnow;

/**
 * Represents a node (City) in the Travelling Salesman Problem
 *
 * @author Paul Moggridge
 * @version 0.1 build 2 "gtr"
 */
public class City {

  private int y;
  private int x;

  public City(int x, int y) {
    this.x = x;
    this.y = y;
  }

  /**
   * Returns the x cor of this city
   *
   * @return int x location of this city
   */
  public int getX() {
    return x;
  }

  /**
   * Returns the y cor of this city
   *
   * @return int y location of this city
   */
  public int getY() {
    return y;
  }

  /**
   * Returns a string representation of this string
   *
   * @return String of the cities cordinates
   */
  public String toString() {
    return "x" + x + " y" + y;
  }
}
