package tspnow;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.util.Random;
import java.lang.reflect.InvocationTargetException;
import javax.swing.SwingUtilities;
import java.util.Observer;
import java.util.Observable;

//reading the xml based map files
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.*; //abstract Document class
import org.w3c.dom.*; //abstract Document class
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;

import tspnow.solvers.*;

/**
 * TSPnow a visual, distributed, experimental TSP Solving Demonstration
 * Application!
 *
 * @author Paul Moggridge
 * @version 4.0.0.0 "se"
 */
public class TSPnow extends Observable {

  //TSP data
  public static CityMap map;
  public static Best runBest;

  //General runtime stuff
  public static final int CORES = Runtime.getRuntime().availableProcessors(); // get the number cores
  private Solver[] solverTracker = new Solver[CORES];                         //record solvers, later we will check the perc
  private Thread[] threadTracker = new Thread[CORES];                         //record threads, later we will check there states
  private Random rand;                                                        //random placing cities

  //event handling - observer pattern
  private UpdateEvent se = new UpdateEvent(); //for collecting "solver events"

  private String version = "4.0.0.0 \"se\"";

  /**
   * Starts TSPnowX
   */
  public static void main(String args[]) {
    Gui gui = new Gui(); //start the gui in a swing thread
    try {
      SwingUtilities.invokeAndWait((Runnable) gui);
    } catch (InterruptedException ex) {
      System.out.println("Probelms loading the GUI in the background! " + ex.getMessage());
    } catch (InvocationTargetException ex) {
      System.out.println("Probelms loading the GUI in the background! " + ex.getMessage());
    }
  }

  public TSPnow() {
    map = new CityMap();
    runBest = new Best(map);
  }

  /**
   * Event handling - Observer pattern
   */
  public class UpdateEvent implements Observer {

    @Override
    public void update(Observable obj, Object arg) {
      if (arg instanceof String) {
        String updateType = (String) arg;
        if (updateType.equals("status")) {
          //pass this event up to the gui or cli
          notifyStatusArea();
        }
        if (updateType.equals("bestupdated")) {
          //pass this event up to the gui or cli
          notifyStatusArea();
          notifyMapArea();
        }
      }
    }
  }

  /**
   * Pass up the event
   */
  private void notifyStatusArea() {
    setChanged();
    notifyObservers("StatusArea");
  }

  /**
   * Pass up the event
   */
  private void notifyMapArea() {
    setChanged();
    notifyObservers("MapArea");
  }

  //#########################################
  // The Solvers
  //#########################################
  /**
   * Launches the genetic algorithm
   */
  public void gaSolve(long seed, int runs, int popSize, int crossOvers, int mutations){
    for (int thread = 0; thread < CORES; thread++) {
      GeneticSolve gaSolver = new GeneticSolve(thread, map, popSize, crossOvers, mutations, seed + (thread + 47), runs, runBest); //make solver objects, note seed+threads otherwise seeds are the same
      solverTracker[thread] = gaSolver;                    //chuck the solver into array for later
      ((Solver) solverTracker[thread]).addObserver(se);       //add observer to allow the gui to know when you update
      threadTracker[thread] = new Thread(gaSolver);        //make the thread and stash it array so we check it later
      (threadTracker[thread]).start();                       //launch their thread capabilities
    }
  }

  /**
   * Launches the brute force method
   */
  public void bruteSolve() {
    int totaliterations = factorial(map.getSize());
    int blockSize = factorial(map.getSize()) / CORES;
    for (int thread = 0; thread < CORES; thread++) {
      //for each thread work out a start and finish point
      int startPoint = (blockSize * thread);
      int endPoint = 0;
      if ((thread + 1) == CORES) {
        endPoint = (blockSize * (thread + 1)) + 1;
      } else {
        endPoint = (blockSize * (thread + 1)) - 1;
      }

      BruteSolve bruteSolver = new BruteSolve(thread, map, startPoint, endPoint, runBest);
      solverTracker[thread] = bruteSolver;                                            //chuck the solver into array for later
      ((Solver) solverTracker[thread]).addObserver(se);                               //add observer to allow the gui to know when you update
      threadTracker[thread] = new Thread(bruteSolver);               //make the thread and stash it array so we check it later
      (threadTracker[thread]).start();                               //launch their thread capabilities
    }
  }

  /**
   * Works out how many brute force runs are required
   */
  private int factorial(int n) {
    int reqRuns = 1;
    for (int i = 1; i <= n; i++) {
      reqRuns = reqRuns * i;
    }
    return reqRuns;
  }

  /**
   * Launches the near solve method
   */
  public void nearSolve() {
    NearSolve nearSolver = new NearSolve(0, map, map.getSize(), runBest);
    ((Solver) nearSolver).addObserver(se);                               //add observer to allow the gui to know when you update
    solverTracker[0] = nearSolver;                                      //chuck the solver into array for later
    threadTracker[0] = new Thread(nearSolver);                          //make the thread and stash it array so we check it later
    (threadTracker[0]).start();                                         //launch their thread capabilities
  }

  /**
   * Initialises a random solver on multiple cores
   *
   * @param s String long to seed the random with
   * @param r String number of runs to conduct
   */
  public void randSolve(long seed, int runs) {
    for (int threads = 0; threads < CORES; threads++) {
      RandSolve randSolver = new RandSolve(threads, map, (seed + threads), runs, runBest); //make solver objects, note seed+threads otherwise seeds are the same
      solverTracker[threads] = randSolver;                                            //chuck the solver into array for later
      ((Solver) solverTracker[threads]).addObserver(se);                               //add observer to allow the gui to know when you update
      threadTracker[threads] = new Thread(randSolver);                                //make the thread and stash it array so we check it later
      (threadTracker[threads]).start();                                               //launch their thread capabilities
    }
  }

  //########################################
  // Building Cities etc ...
  //#########################################
  /**
   * Generates random cities
   *
   * @param mx String number defining the max xcor of the area to build in
   * @param my String number defining the max ycor of the area to build in
   * @param n String number or how many cities to build
   * @param s String number to use as the random seed
   *
   * @exception NumberFormatException if the input String are not number
   * @exception TSPnowInputException id the number don't make sense
   *
   */
  public void randCity(String mx, String my, String n, String s)
      throws NumberFormatException, TSPnowInputException {
    int numberOfCitiesToBuild = 0;
    long randSeed = 0l;
    int maxXCor = 0;
    int maxYCor = 0;

    try {
      numberOfCitiesToBuild = Integer.parseInt(n);
      randSeed = Long.parseLong(s);
      maxXCor = (int) Math.round(Double.parseDouble(mx));
      maxYCor = (int) Math.round(Double.parseDouble(my));
    } catch (NumberFormatException nfe) {
      throw nfe;
    }

    //check the numbers make sense
    if (maxXCor < 100) //make sure the x cor is big enough
    {
      throw new TSPnowInputException("Invalid Max X Cordinate, the window your trying to create the cities in is too narrow!");
    }

    if (maxYCor < 100) //make sure the y cor is big enough
    {
      throw new TSPnowInputException("Invalid Max Y Cordinate, the window your trying to create the cities in is too short!");
    }

    Random rand = new Random(randSeed);

    for (int i = 0; i < numberOfCitiesToBuild; i++) {
      addCity(rand.nextInt(maxXCor - 80) + 40, rand.nextInt(maxYCor - 80) + 40);
    }
  }

  /**
   * Adds a city to the citymap
   */
  public void addCity(String x, String y)
      throws NumberFormatException {
    try {
      addCity(Integer.parseInt(x), Integer.parseInt(y));
    } catch (NumberFormatException nfe) {
      throw nfe;
    }
  }

  public void addCity(int x, int y) {
    map.addCity(new City(x, y));
    notifyMapArea(); //Let the user interface know the map has been updated
    clearBest();
  }

  /**
   * Save a map file, map are based on XML
   *
   * @param fname String name of file to save to
   */
  public void saveFile(String fname)
      throws ParserConfigurationException, TransformerException, IOException {
    Document dom;
    try {
      DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder(); //create a xml document builder
      dom = db.newDocument(); //create a document to populate with the map

      Element rootEle = dom.createElement("tspmap"); //create the base node <tspmap>

      for (int c = 0; c < map.getSize(); c++) {
        //add each city
        Element e = dom.createElement("city");
        e.setAttribute("id", c + "");
        e.setAttribute("xcor", map.getCity(c).getX() + "");
        e.setAttribute("ycor", map.getCity(c).getY() + "");
        rootEle.appendChild(e);
      }

      dom.appendChild(rootEle);

      try {
        Transformer tr = TransformerFactory.newInstance().newTransformer();
        tr.setOutputProperty(OutputKeys.INDENT, "yes");
        tr.setOutputProperty(OutputKeys.METHOD, "xml");
        tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        //tr.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, "roles.dtd");
        tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

        // send DOM to file
        tr.transform(new DOMSource(dom),
            new StreamResult(new FileOutputStream(fname)));
      } catch (TransformerException te) {
        throw te;
      } catch (IOException ioe) {
        throw ioe;
      }
    } catch (ParserConfigurationException pce) {
      throw pce;
    }
  }

  /**
   * Open a map file, map are based on XML
   *
   * @param fname String name of file to open
   */
  public void openFile(String fname)
      throws ParserConfigurationException, IOException, SAXException, NumberFormatException {
    Document doc;
    try {
      DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();

      doc = db.parse(fname);
      doc.getDocumentElement().normalize();

      //read each city details from the xml
      for (int cx = 0; cx < doc.getElementsByTagName("city").getLength(); cx++) {
        int x = (int) Math.round(Double.parseDouble(doc.getElementsByTagName("city").item(cx).getAttributes().getNamedItem("xcor").getNodeValue()));
        int y = (int) Math.round(Double.parseDouble(doc.getElementsByTagName("city").item(cx).getAttributes().getNamedItem("ycor").getNodeValue()));
        map.addCity(new City(x, y));
      }
    } catch (ParserConfigurationException pce) {
      throw pce;
    } catch (IOException ioe) {
      throw ioe;
    } catch (SAXException saxe) {
      throw saxe;
    } catch (NumberFormatException nfe) {
      throw nfe;
    }
  }

  /**
   * Imports a CSV file where th
   */
  public void importCSV(String fname)
      throws FileNotFoundException, Exception {
    try {
      File toImport = new File(fname);
      Scanner fileReader = new Scanner(toImport);

      do {
        String coords = fileReader.nextLine();
        String longditude = coords.split(",")[0];
        String latitude = coords.split(",")[1];

        int x = (int) Math.round(Double.parseDouble(longditude));
        int y = (int) Math.round(Double.parseDouble(latitude));
        map.addCity(new City(x, y));
      } while (fileReader.hasNext());
    } catch (FileNotFoundException fnfe) {
      throw fnfe;
    } catch (Exception ex) {
      throw ex;
    }
  }

  //##################################################
  // Other stuff ...
  //##################################################
  /**
   * Works the number of solver threads not in the terminated state
   *
   * @return the number of running threads as integer
   */
  public int getNumberRunningThreads() {
    int activeThreadCount = 0;
    for (int threadCheck = 0; threadCheck < threadTracker.length; threadCheck++) {
      if (threadTracker[threadCheck] != null) {
        if (threadTracker[threadCheck].getState() != Thread.State.TERMINATED) {
          activeThreadCount++;
        }
      }
    }
    return activeThreadCount;
  }

  /**
   * Takes all the threads and calculates a average complete percentage
   *
   * @return double percentage that all the non-terminated threads are at
   */
  public double getPercentComplete() {
    double totalProgress = 0;
    for (int percentCheck = 0; percentCheck < solverTracker.length; percentCheck++) {
      if (solverTracker[percentCheck] != null) {
        totalProgress = totalProgress + (((solverTracker[percentCheck].getProgress() + 0.0) / (solverTracker[percentCheck].getRuns() + 0.0)) * 100);
      }
    }
    if (getNumberRunningThreads() == 0) {
      return 100.0;
    }
    return totalProgress / (getNumberRunningThreads() + 0.0);
  }

  public String getVersion() {
    return "TSPnow " + version;
  }

  /**
   * resets the program
   */
  public void reset() {
    //reset the map
    map = new CityMap();
    runBest = new Best(map);
  }

  public Best getBest() {
    return runBest;
  }

  public void clearBest() {
    runBest.clear();
    notifyStatusArea();
  }

  public CityMap getMap() {
    return map;
  }

  //######################################
  // Custom TSP Exceptions
  //######################################
  public class TSPnowInputException extends Exception {

    public TSPnowInputException() {
      super();
    }

    public TSPnowInputException(String message) {
      super(message);
    }
  }
}
