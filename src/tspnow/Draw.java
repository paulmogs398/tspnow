package tspnow;

import javax.swing.JPanel;
import java.awt.*;
import java.awt.Color;

//loading the welcome image
import java.io.InputStream;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.io.IOException;

/**
 * @author Paul
 * @version 4.0.0.1 se
 */
public class Draw extends JPanel {

  private CityMap map;            //the map to be drawn
  private int mapSize;            //the map size to be obtain from the map
  private int[] bestToDraw;       //the solution to draw, typically the best
  private Color lineColor;        //the color to draw the "line" solution with
  private Color cityColor;        //the color to draw the cities with
  private Color backGround;       //the color to draw the background as
  private boolean drawNumbers = true;     //whether of the id numbers are draw above the cities

  public Draw(CityMap map, int[] best, Color line, Color city, Color back) {
    this.map = map;
    mapSize = map.getSize();
    bestToDraw = best;

    //set colors
    backGround = back;
    cityColor = city;
    lineColor = line;

  }

  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    this.setBackground(backGround);

    boolean drawWelcome = true;

    //draw them cities
    g.setColor(cityColor);
    if (mapSize > 0) {
      drawWelcome = false;
      for (int cityPen = 0; cityPen < mapSize; cityPen++) {
        g.fillRect((map.getCity(cityPen).getX() - 2) + 15, (map.getCity(cityPen).getY() - 2) + 15, 4, 4); //-2 to centralise line overdrawn later and +15 to steer clear of edge
        if (drawNumbers) {
          g.drawString("" + cityPen, (map.getCity(cityPen).getX() - 2 + 10), (map.getCity(cityPen).getY() - 2 + 10));
        }
      }
    } else {
      g.drawString("No map built yet...", 10, 10);
    }

    //draw the best solution
    g.setColor(lineColor);
    if (bestToDraw != null && bestToDraw.length > 1 && bestToDraw.length == map.getSize()) {
      drawWelcome = false;
      for (int bestDrawTime = 0; bestDrawTime < bestToDraw.length - 1; bestDrawTime++) {
        g.drawLine((map.getCity(bestToDraw[bestDrawTime]).getX()) + 15, (map.getCity(bestToDraw[bestDrawTime]).getY()) + 15, (map.getCity(bestToDraw[bestDrawTime + 1]).getX()) + 15, (map.getCity(bestToDraw[bestDrawTime + 1]).getY()) + 15);
      }
    } else {
      g.drawString("No solution calculated yet...", 10, 20);
    }

    if (drawWelcome) {
      InputStream welcomeGraphicLoad = getClass().getResourceAsStream("res/images/welcome.png");
      if (welcomeGraphicLoad != null) {
        try {
          BufferedImage welcomeGraphic = ImageIO.read(welcomeGraphicLoad);
          g.drawImage(welcomeGraphic, 200, 80, null);
        } catch (IOException ioe) {
        };
      }
    }
  }

  /**
   * The new (typically best) solution to draw onto cities
   *
   * @param nodes int[] of cities in order to visit
   */
  public void updateSolution(int[] nodes) {
    bestToDraw = nodes;     //get best!
    //repaint();
  }

  /**
   *
   * @param map CityMap (normally populated with cities) to draw onto the screen
   */
  public void updateMap(CityMap map) {
    mapSize = map.getSize();
    this.map = map;          //get new map
    //repaint();
  }

  public void setDrawNumbers(boolean drawNumbers) {
    this.drawNumbers = drawNumbers;
  }

  public boolean getDrawNumbers() {
    return drawNumbers;
  }

  public void setLineColor(Color color) {
    lineColor = color;
  }

  public void setCityColor(Color color) {
    cityColor = color;
  }

  public void setBackGroundColor(Color color) {
    backGround = color;
  }

  public Color getLineColor() {
    return lineColor;
  }

  public Color getCityColor() {
    return cityColor;
  }

  public Color getBackgroundColor() {
    return backGround;
  }
}
